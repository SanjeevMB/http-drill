const http = require('http');
const fs = require('fs');
const uuid = require('uuid');
const PORT = process.env.PORT || 5000;

const server = http.createServer((request, response) => {

    let delay = 3;

    if (request.method === 'GET' && request.url === '/html') {

        fs.readFile('martinFowler.html', (error, data) => {

            if (error) {

                response.writeHead(500);

                response.end('Error in page loading martinFowler.html');

            } else {

                response.setHeader('Content-Type', 'text/html');

                response.writeHead(200);

                response.end(data);

            }

        });

    } else if (request.url === '/json') {

        fs.readFile('randomData.json', (error, data) => {

            if (error) {

                response.writeHead(500);

                response.end('Error in page loading random.json');

            } else {

                response.setHeader('Content-Type', 'text/json');

                response.writeHead(200);

                response.end(data);

            }

        });

    } else if (request.url === '/uuid') {

        let id = uuid.v4();

        let uuidObj = JSON.stringify({ uuid: id });

        response.setHeader('Content-Type', 'text/json');

        response.writeHead(200);

        response.end(uuidObj);

    } else if (request.url.includes('/status')) {

        let statusCode = request.url.split('/')
            .pop();

        try {

            response.setHeader('Content-Type', 'text/plain');

            response.writeHead(200);

            response.write(http.STATUS_CODES[statusCode]);

        } catch {

            response.setHeader(404);

            response.writeHead('Content-Type', 'text/plain');

            response.end('Bad request');

        }

        response.end();

    } else if (request.url === `/delay/${delay}`) {

        setTimeout(() => {

            response.setHeader('Content-Type', 'text/plain');

            response.writeHead(200);

            response.end(`You are  waiting from ${delay} seconds `);

        }, `${delay}` * 1000);

    } else {

        response.writeHead(404);
        response.end('Page not found');

    }

});

server.listen(PORT, 'localhost', () => {

    console.log('Listening server on port 5000');

});